$('.Material').focusout(function(){
	if($('input[type="text"]').val()!='')
	{
		$('input[type="text"]~label').addClass('activeLabel');
	}
	else{
		$('input[type="text"]~label').removeClass('activeLabel');
	}
	if($('input[type="password"]').val()!='')
	{
		$('input[type="password"]~label').addClass('activeLabel');
	}
	else{
		$('input[type="password"]~label').removeClass('activeLabel');
	}
});

function invalid(){
	$('.error').fadeIn('slow');
	setTimeout(function(){
		$('.error').fadeOut('slow');
	},4000);
	$('input').css("border-bottom-color","red");
}

function valid(){
	invalid();
	return false;
}
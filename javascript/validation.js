			$(document).ready(function(){

				$('form button').click(function(ev){
					ev.preventDefault();
				});

				$("#personalGroup").addClass("tabActive");
			/*	$("#professionalGroup").click(function(){
					$("#personalGroup").removeClass("tabActive");
					$("#locationGroup").removeClass("tabActive");
					$(this).addClass("tabActive");

					$("#personalDetails").hide();
					$("#locationAndResumeDetails").hide();
					$("#wholeProfessionalDetails").show();
					$("#professionalDetails").show();
				});
				$("#locationGroup").click(function(){
					$("#personalGroup").removeClass("tabActive");
					$("#professionalGroup").removeClass("tabActive");
					$(this).addClass("tabActive");


					$("#personalDetails").hide();
					$("#wholeProfessionalDetails").hide();
					$("#locationAndResumeDetails").show();
				});
			
				$("#personalGroup").click(function(){
					$("#locationGroup").removeClass("tabActive");
					$("#professionalGroup").removeClass("tabActive");
					$(this).addClass("tabActive");


					$("#locationAndResumeDetails").hide();
					$("#wholeProfessionalDetails").hide();
					$("#personalDetails").show();
				});
			*/
				$("#nextProfessional").click(function(){
					var flag = validatePersonal();
					if(flag)
					{
						$("#personalGroup").removeClass("tabActive");
						$("#locationGroup").removeClass("tabActive");
						$("#professionalGroup").addClass("tabActive");

						$("#personalDetails").hide();
						$("#locationAndResumeDetails").hide();
						$("#wholeProfessionalDetails").show();
						$("#professionalDetails").show();
					}
					else{
						triggerError("Please enter all required field");
					}
				});
			
				$("#nextLocation").click(function(){
					var flag = validateProfessionalFresher();
					if(flag)
					{
						$("#personalGroup").removeClass("tabActive");
						$("#professionalGroup").removeClass("tabActive");
						$("#locationGroup").addClass("tabActive");


						$("#personalDetails").hide();
						$("#wholeProfessionalDetails").hide();
						$("#locationAndResumeDetails").show();
					}
					else{
						triggerError("Please enter all required field");
					}
				});

				$("#nextLocation1").click(function(){
					var flag = validateProfessionalLessThanAYear();
					if(flag)
					{
						$("#personalGroup").removeClass("tabActive");
						$("#professionalGroup").removeClass("tabActive");
						$("#locationGroup").addClass("tabActive");


						$("#personalDetails").hide();
						$("#wholeProfessionalDetails").hide();
						$("#locationAndResumeDetails").show();
					}
					else{
						triggerError("Please enter all required field");
					}

				});

				$("#nextLocation2").click(function(){
					var flag = validateProfessionalMoreThanAYear();
					if(flag)
					{
						$("#personalGroup").removeClass("tabActive");
						$("#professionalGroup").removeClass("tabActive");
						$("#locationGroup").addClass("tabActive");


						$("#personalDetails").hide();
						$("#wholeProfessionalDetails").hide();
						$("#locationAndResumeDetails").show();
					}
					else{
						triggerError("Please enter all required field");
					}
				});

				$("#workExperience").change(function(){
					if($(this).val()==0)
					{
						$("#currentCompanyGroup").slideUp(1000);
					}
					else{
						$("#currentCompanyGroup").slideDown(1000);
					}
				});

				$("#name").focusout(function(){
					var flag = validateName(this);
					if(!flag){
						triggerError("Please enter valid name");
					}
				});

				$("#phone").focusout(function(){
					var flag = validateNumber(this);
					if(!flag)
					{
						triggerError("Please enter valid phone number");
					}
				});

				$("#email").focusout(function(){
					var flag = validateEmail(this);
					if(!flag)
					{
						triggerError("Please enter valid email");
					}
				});

				$("#dob").focusout(function(){
					var flag = validateDob(this);
					if(!flag)
					{
						triggerError("Please enter valid date of birth in dd/mm/yyyy between year 1955 and 1997");
					}
				});

				$("#linkedin").focusout(function(){
					var flag = validateLinkedIn(this);
					if(!flag)
					{
						triggerError("Please enter valid LinkedIn Link");
					}
				});

				$("#schoolBoardResult").focusout(function(){
					var flag = validatePercentage(this);
					if(!flag)
					{
						triggerError("Please enter valid Percentage above 30");
					}
				});

				$("#schoolSeniorBoardResult").focusout(function(){
					var flag = validatePercentage(this);
					if(!flag)
					{
						triggerError("Please enter valid Percentage above 30");
					}
				});

				$("#lessSchoolBoardResult").focusout(function(){
					var flag = validatePercentage(this);
					if(!flag)
					{
						triggerError("Please enter valid Percentage above 30");
					}
				});

				$("#lessSchoolSeniorBoardResult").focusout(function(){
					var flag = validatePercentage(this);
					if(!flag)
					{
						triggerError("Please enter valid Percentage above 30");
					}
				});

				$("#graduationPersentage").focusout(function(){
					var flag = validatePercentage(this);
					if(!flag)
					{
						triggerError("Please enter valid Percentage above 30");
					}
				});

				$("#postGraduationPersentage").focusout(function(){
					var flag = validatePercentage(this);
					if(!flag)
					{
						triggerError("Please enter valid Percentage above 30");
					}
				});

				$("#lessGraduationPersentage").focusout(function(){
					var flag = validatePercentage(this);
					if(!flag)
					{
						triggerError("Please enter valid Percentage above 30");
					}
				});

				$("#lessPostGraduationPersentage").focusout(function(){
					var flag = validatePercentage(this);
					if(!flag)
					{
						triggerError("Please enter valid Percentage above 30");
					}
				});

				$("#college").focusout(function(){
					var flag = validateName(this);
					if(!flag)
					{
						triggerError("Please enter valid College Name");
					}
				});

				$("#pcollege").focusout(function(){
					var flag = validateName(this);
					if(!flag)
					{
						triggerError("Please enter valid Post Graduation College Name");
					}
				});

				$("#lessCurrentCompany").focusout(function(){
					var flag = validateName(this);
					if(!flag)
					{
						triggerError("Please enter valid Company Name");
					}
				});

				$("#lessCollege").focusout(function(){
					var flag = validateName(this);
					if(!flag)
					{
						triggerError("Please enter valid Graduation College Name");
					}
				});

				$("#lessPCollege").focusout(function(){
					var flag = validateName(this);
					if(!flag)
					{
						triggerError("Please enter valid Post Graduation College Name");
					}
				});

				$("#moreCurrentCompany").focusout(function(){
					var flag = validateName(this);
					if(!flag)
					{
						triggerError("Please enter valid Company Name");
					}
				});

				$("#year").focusout(function(){
					var flag = validateYear(this);
					if(!flag)
					{
						triggerError("Please enter valid Year of graduation");
					}
				});

				$("#pyear").focusout(function(){
					var flag = validateYear(this);
					if(!flag)
					{
						triggerError("Please enter valid Year of post graduation");
					}
				});

				$("#lessYear").focusout(function(){
					var flag = validateYear(this);
					if(!flag)
					{
						triggerError("Please enter valid year of graduation");
					}
				});

				$("#lessPYear").focusout(function(){
					var flag = validateYear(this);
					if(!flag)
					{
						triggerError("Please enter valid year of post graduation");
					}
				});

				$("#graduationStream").focusout(function(){
					var flag = validateDropdown(this);
					if(!flag)
					{
						triggerError("Please select correct stream of graduation");
					}
				});

				$("#postGraduationStream").focusout(function(){
					var flag = validateDropdown(this);
					if(!flag)
					{
						triggerError("Please select correct stream of post graduation");
					}
				});

				$("#lessGraduationStream").focusout(function(){
					var flag = validateDropdown(this);
					if(!flag)
					{
						triggerError("Please select correct graduation stream");
					}
				});

				$("#lessPostGraduationStream").focusout(function(){
					var flag = validateDropdown(this);
					if(!flag)
					{
						triggerError("Please select valid post graduation stream");
					}
				});

				$("#lessSkills0").focusout(function(){
					var flag = validateDropdown(this);
					if(!flag)
					{
						triggerError("Please select right skills");
					}
				});

				$("#lessSkills1").focusout(function(){
					var flag = validateDropdown(this);
					if(!flag)
					{
						triggerError("Please select right skills");
					}
				});

				$("#lessSkills2").focusout(function(){
					var flag = validateDropdown(this);
					if(!flag)
					{
						triggerError("Please select right skills");
					}
				});

				$("#moreSkills0").focusout(function(){
					var flag = validateDropdown(this);
					if(!flag)
					{
						triggerError("Please select right skills");
					}
				});

				$("#moreSkills1").focusout(function(){
					var flag = validateDropdown(this);
					if(!flag)
					{
						triggerError("Please select right skills");
					}
				});

				$("#moreSkills2").focusout(function(){
					var flag = validateDropdown(this);
					if(!flag)
					{
						triggerError("Please select right skills");
					}
				});

				$("#currentLocation").focusout(function(){
					var flag = validateDropdown(this);
					if(!flag)
					{
						triggerError("Please select correct location");
					}
				});				

				$("#preferredJoiningCenter").focusout(function(){
					var flag = validateDropdown(this);
					if(!flag)
					{
						triggerError("Please select valid preferred location");
					}
				});

				$("#resume").focusout(function(){
					var flag = validateUpload(this);
					if(!flag)
					{
						triggerError("Please upload an pdf or doc file");
					}
				});

				$("#continue").click(function(){
					$("#form-body")[0].reset();
					$('.customAlert').fadeOut('fast');
					$('.overlay-alert').fadeOut('fast');
					$("input").removeClass("errorText");
					$("input").removeClass("correctText");
					$("select").removeClass("formSelectRight");
					$("select").removeClass("formSelectError");
					$("input[type='file']").removeClass("formSelectRight");
					$("input[type='file']").removeClass("formSelectError");
					$("input[name='experience']").val('');
					$("#professionalDetailsMoreThanYear").hide();
					$("#professionalDetailsLessThanYear").hide();
					$("#professionalDetailsFresher").hide();
					$("#fresher").removeClass('exp-active');
					$("#lesserYear").removeClass('exp-active');
					$("#moreYear").removeClass('exp-active');

					$("#locationGroup").removeClass("tabActive");
					$("#professionalGroup").removeClass("tabActive");
					$("#personalGroup").addClass("tabActive");


					$("#locationAndResumeDetails").hide();
					$("#wholeProfessionalDetails").hide();
					$("#personalDetails").show();
				});

			});
			
				function validateName(el){
					var flag = validateEmpty(el);
					if(flag)
					{
						var pattern = /[^a-zA-Z ]/;
						if(pattern.test($(el).val())) {
							$(el).removeClass("correctText");
							$(el).addClass("errorText");
							return false;
						}
						$(el).removeClass("errorText");
						$(el).addClass("correctText");
						return true;
					} 
				}

				function validateNumber(el){
					var pattern = /^\(?(\d{3})\)?[- ]?(\d{3})[- ]?(\d{4})$/;
					if(pattern.test($(el).val()))
					{
						$(el).removeClass("errorText");
						$(el).addClass("correctText");
						return true;
					}
					$(el).removeClass("correctText");
					$(el).addClass("errorText");
					return false;
				}

				function validatePercentage(el){
					var pattern = /^100$|^\d{0,2}(\.\d{1,2})? *%?$/;
					var flag = validateEmpty(el);
					var rightPerc = validateCorrectPercentage(el);
					if(flag&&rightPerc)
					{
						if(pattern.test($(el).val()))
						{
							$(el).removeClass("errorText");
							$(el).addClass("correctText");
							return true;
						}
						$(el).removeClass("correctText");
						$(el).addClass("errorText");
						return false;
					}
					$(el).removeClass("correctText");
					$(el).addClass("errorText");
					return false;
				}

				function validateEmail(el){
					var pattern = /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/;
					if(pattern.test($(el).val()))
					{
						$(el).removeClass("errorText");
						$(el).addClass("correctText");
						return true;
					}
					$(el).removeClass("correctText");
					$(el).addClass("errorText");
					return false;
				}

				function validateLinkedIn(el){
					var pattern = /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/ ;
					if(pattern.test($(el).val()))
					{
						$(el).removeClass("errorText");
						$(el).addClass("correctText");
						return true;
					}
					$(el).removeClass("correctText");
					$(el).addClass("errorText");
					return false;
				}

				function validateDob(el){
					var pattern = /^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/;
					var properDate = validateProperDate(el);
					if(pattern.test($(el).val()))
					{
						if(properDate)
						{
							$(el).removeClass("errorText");
							$(el).addClass("correctText");
							return true;
						}
						$(el).removeClass("correctText");
						$(el).addClass("errorText");
						return false;
					}
					$(el).removeClass("correctText");
					$(el).addClass("errorText");
					return false;
				}

				function validateYear(el){
					var pattern = /(19[789]\d|20[01]\d)/;
					if(pattern.test($(el).val()))
					{
						$(el).removeClass("errorText");
						$(el).addClass("correctText");
						return true;
					}
					$(el).removeClass("correctText");
					$(el).addClass("errorText");
					return false;
				}

				function validateEmpty(el){
					var len = $(el).val().trim().length;
					if(len<1)
					{
						$(el).removeClass("correctText");
						$(el).addClass("errorText");
						return false;
					}
					$(el).removeClass("errorText");
					$(el).addClass("correctText");
					return true;
				}

				function validateCorrectPercentage(el){
					var per = $(el).val().trim();
					if(per<30)
					{
						$(el).removeClass("correctText");
						$(el).addClass("errorText");
						return false;
					}
					$(el).removeClass("errorText");
					$(el).addClass("correctText");
					return true;	
				}

				function validateDropdown(el){
					if($(el).val()==-1)
					{
						$(el).removeClass("formSelectRight");
						$(el).addClass("formSelectError");
						return false;
					}
					$(el).removeClass("formSelectError");
					$(el).addClass("formSelectRight");
					return true;
				}

				function validateUpload(el){
					var val = $(el).val();
					var file_type = val.substr(val.lastIndexOf('.')).toLowerCase();
					if (file_type  === '.pdf'|| file_type === '.doc') 
					{
						$(el).removeClass("formSelectError");
						$(el).addClass("formSelectRight");
						return true;
					}
					$(el).removeClass("formSelectRight");
					$(el).addClass("formSelectError");
					return false;
				}

				function beforeSubmit(){
					var flag= validateUpload($("#resume"));
					if(!flag){
						triggerError("Please upload an pdf or doc file");
						return false;
					} 
					else {
						$('.overlay-alert').fadeIn('slow', function(){
							$('.alert-heading').text('Alert');
							$('#alertContent > span').text('Hi '+$("#name").val().trim()+' your profile has been created successfully.');
							$('.customAlert').fadeIn('fast');
						});
					}
					return false;
				}

				function validateProperDate(el){
					var val = $(el).val();
					if((val.lastIndexOf('.')).valueOf()!=-1){
						var file_type = val.substr(val.lastIndexOf('.')+1).valueOf();
						if(file_type<1997&&file_type>1955)
						{
							return true;
						}
						return false;
					}
					else if((val.lastIndexOf('/')).valueOf()!=-1){
						var file_type = val.substr(val.lastIndexOf('/')+1).valueOf();
						if(file_type<1997&&file_type>1955)
						{
							return true;
						}
						return false;
					}
					else if((val.lastIndexOf('-')).valueOf()!=-1){
						var file_type = val.substr(val.lastIndexOf('-')+1).valueOf();
						if(file_type<1997&&file_type>1955)
						{
							return true;
						}
						return false;	
					}
					return false;
				}

			function setExperience(el) {
				$('input[name="experience"]').val($(el).attr('data-experience'));
				$(el).siblings().removeClass('exp-active');
				$(el).addClass('exp-active');
				if($(el).attr('data-experience')=="FRESHER")
				{
					$("#professionalDetailsMoreThanYear").hide();
					$("#professionalDetailsLessThanYear").hide();
					$("#professionalDetailsFresher").slideDown(3000);
				}
				else if($(el).attr('data-experience')=="LESS_THAN_A_YEAR")
				{
					$("#professionalDetailsFresher").hide();
					$("#professionalDetailsMoreThanYear").hide();
					$("#professionalDetailsLessThanYear").slideDown(3000);
				}
				else if($(el).attr('data-experience')=="MORE_THAN_A_YEAR")
				{
					$("#professionalDetailsLessThanYear").hide();
					$("#professionalDetailsFresher").hide();
					$("#professionalDetailsMoreThanYear").slideDown(3000);
				}
			};

			function triggerError(message){
				$('.error').slideUp('fast');
				$('#error-text').text(message);
				$('.error').slideDown('fast');
				scrollTo(top);
				setTimeout(function() {
					$('.error').slideUp('fast');
				},3000);
			}

			function hideError() {
				$('.error').slideUp('fast');
			}

			function validatePersonal(){
				var check = [(validateName($("#name"))),(validateNumber($("#phone"))),(validateEmail($("#email"))),(validateNumber($("#phone"))),(validateDob($("#dob")))];
				if(check[0] &&check[1]&&check[2]&&check[3]&&check[4]) {
					return true;
				}
				return false;
			}

			function validateProfessionalFresher(){
				var check = [(validatePercentage($("#schoolBoardResult"))) ,(validatePercentage($("#schoolSeniorBoardResult"))),(validatePercentage($("#graduationPersentage"))),(validateName($("#college"))),(validateYear($("#year"))),(validateDropdown($("#graduationStream")))];
				if(check[0]&&check[1]&&check[2]&&check[3]&&check[4]&&check[5]) {
					return true;
				}
				return false;
			}

			function validateProfessionalLessThanAYear(){
				var check = [(validatePercentage($("#lessSchoolBoardResult"))) ,(validatePercentage($("#lessSchoolSeniorBoardResult"))),(validatePercentage($("#lessGraduationPersentage"))),(validateName($("#lessCollege"))),(validateYear($("#lessYear"))),(validateDropdown($("#lessGraduationStream"))),(validateDropdown($("#lessSkills0")))];
				if(check[0]&&check[1]&&check[2]&&check[3]&&check[4]&&check[5]&&check[6]) {
					return true;
				}
				return false;
			}

			function validateProfessionalMoreThanAYear(){
				if(validateDropdown($("#moreSkills0"))) {
					return true;
				}
				return false;
			}

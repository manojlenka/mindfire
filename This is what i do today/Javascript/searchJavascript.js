$('.back').click(function(){
	$(this).css('display','none');
	$('.open').text('');
	$('.imageOpen').attr('src','');
	$('.imageOpen').attr('alt','');
});

$('.close').click(function(){
	$('.form-hidden').css("display","none");
});

$('#bird').click(function(){
	$('.form-hidden').css("display","block");
});

$('#writelogo').click(function(){
	$('.form-hidden').css("display","block");
});

function animateParagraph(el)
{
	$('.imageOpen').attr('src',$(el).children('.image-inside').children('.image-box').attr('src'));
	$('.imageOpen').attr('alt',$(el).children('.image-inside').children('.image-box').attr('alt'));
	$('.open').text($(el).text());
	$('.back').css("display","block");
}

function animateLabel(el)
{
	if($(el).val()!=''){
		$(el).siblings('label').addClass('activeLabel');
	}
	else{
		$(el).siblings('label').removeClass('activeLabel');
	}
}

function invalid(){
	$('.error').fadeIn('slow');
	setTimeout(function(){
		$('.error').fadeOut('slow');
	},4000);
	$('input').css("border-bottom-color","red");
}

function valid(){
	invalid();
	return false;
}

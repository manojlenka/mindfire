using System;
class Fibonacci{
	public static void Main(){
		//int i;
		long n=0;
		Console.WriteLine("Enter number whose fibonacci is to be calculated: ");
		string str=Console.ReadLine();
		bool b = Int64.TryParse(str, out n);
		if(b)
		{
			long[] result=FibNumber(n);
			Console.WriteLine("Result of fibonnaci number: ");
			//for(i=0;i<n;i++)
			{
				Console.Write(result[n-1]+" ");
			}
		}
		else
		{
			Console.WriteLine("Please insert number");
		}
		Console.Read();
		
	}
	public static long[] FibNumber(long number)
	{
		long[] arr=new long[1000];
		int i;
		arr[0]=1L;
		arr[1]=1L;
		if(number>2)
		{
			for(i=3;i<=number;i++)
			{
				arr[i-1]=arr[i-2]+arr[i-3];
			}
		}
		return arr;
	}
}
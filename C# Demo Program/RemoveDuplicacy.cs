using System;

class RemoveDuplicacy
{
	public static void Main()
	{
		Console.WriteLine("Enter a string: ");
		string test = Console.ReadLine();
		string output = RemoveDuplicate(test);
		Console.WriteLine("After removing duplicacy: "+output);
		Console.Read();
	}
	public static string RemoveDuplicate(string str)
	{
		bool flag = true;
		string result=string.Empty;
		if(str.Length>0)
			result = str[0].ToString();
		for(int i=0;i<str.Length;i++)
		{
			flag = false;
			for(int j=0;j<result.Length;j++)
			{
				if(result[j]==str[i])
				{
					flag = true;
					break;
				}
			}
			if(!flag)
				result = result + str[i].ToString();
		}
		return result;
	}
}
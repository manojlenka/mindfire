using System;
class Anagram{
	public static void Main(){
		string orig,conv;
		int i,j;
		bool found=false;
		Console.WriteLine("Enter String: ");
		orig=Console.ReadLine().Trim().ToLower();
		Console.WriteLine("Enter String to be checked anagram: ");
		conv=Console.ReadLine().Trim().ToLower();
		char[] ch1=orig.ToCharArray();
		char[] ch2=conv.ToCharArray();
		if(orig.Length==conv.Length)
		{
			for(i=0;i<ch1.Length;i++)
			{
				found=false;
				if(ch1[i]!='\0')
				{
					for(j=0;j<ch2.Length;j++)
					{
						if(ch2[j]!='\0')
						{
							if(ch1[i]==ch2[j])
							{
								found=true;
								ch1[i]='\0';
								ch2[j]='\0';
								break;
							}
						}
					}
				}
				if(!found)
				{
					Console.WriteLine("Not an anagram string");
					break;
				}
			}
			if(i==ch1.Length)
			{
				Console.WriteLine("String is an anagram");
			}
		}
		else
		{
			Console.WriteLine("String is not anagram");
		}
		Console.Read();
	}
}
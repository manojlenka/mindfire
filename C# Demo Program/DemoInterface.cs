using System;
interface IMessage
{
 void SetConnection(string conn);
 string GetConnection();
 void Read(string a);
 string Write( );
 void Send(string b);
 string Receive();
}
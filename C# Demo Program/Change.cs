using System;

class Change
{
	public static void Main(string[] args){
		Console.Write("Enter first string: ");
		string str = Console.ReadLine().ToLower();
		Console.Write("Enter second string: ");
		string element = Console.ReadLine().ToLower();
		int index= str.IndexOf(element);
		string result=(index!=-1)?"yes":"no";
		Console.WriteLine("Is this element is present in string {0}: {1}",element,result);
		Console.Read();
	}
}
using System;

class LargestSum
{
	public static void Main()
	{
		Console.WriteLine("Enter array of numbers to find largest sum: ");
		string str = Console.ReadLine();
		int output = MaxSum(str);
		Console.WriteLine("Max sum is: "+ output);
		Console.Read();
	}
	public static int MaxSum(string test)
	{
		int sum=0;
		int i,j,max=0;
		if(test.Length>0)
		{
			string[] str=(test).Split(' ');
			int[] arr = Array.ConvertAll<string,int>(str,int.Parse);
			if(arr.Length>0)
				max = arr[0];
			for(i=1;i<arr.Length;i++)
			{
				if(max<arr[i])
					max = arr[i];
			}
			for(i=0;i<arr.Length-1;i++)
			{
				sum=0;
				for(j=i;j<arr.Length;j++)
				{
					sum = sum+arr[j];
					if(sum>max)
						max = sum;
				}
			}
			return max;
		}
		return max;
	}
}
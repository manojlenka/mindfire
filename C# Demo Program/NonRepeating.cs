using System;
class NonRepeating{
	public static void Main(){
		string ch=string.Empty;
		Console.WriteLine("Input string:");
		string str = Console.ReadLine().Trim().ToLower();
		ch = Repeat(str);
		Console.WriteLine(ch);
		Console.Read();
	}
	public static string Repeat(string s)
	{
		long i,j;
		bool flag=false;
		char ch=' ';
		char[] a=s.ToCharArray();
		for(i=0L;i<a.Length;i++)
		{
			flag=true;
			if(a[i]!=' ')
			{
				for(j=i+1;j<a.Length;j++)
				{
					if(a[j]!=' ')
					{
						if(a[i]==a[j])
						{
							flag=false;
							a[j]=' ';
						}
					}
				}
				if(flag==true)
				{
					ch=a[i];
					return ch.ToString();
				}
			}
		}
		return "-1";
	}
}
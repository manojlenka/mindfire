using System;
class A{
	private static A instance=new A();
	private A(){}
	public static A GetInstances(){
	/*	if(instance==null)
		{
			instance=new A();
		}	
	*/
		return instance;
	}
}
class Inherit
{
	public static void Main(){
		A a=A.GetInstances();
		A b=A.GetInstances();
		Console.WriteLine(a.GetHashCode()+"  "+b.GetHashCode());
	}
}
// C program to print all permutations with duplicates allowed
using System;
using System.Text;

class Permutation
{
	/* Function to swap values at two pointers */
	public static void swap(ref StringBuilder x, ref StringBuilder y)
	{
		StringBuilder temp;
		temp = x;
		x = y;
		y = temp;
	}
	
	/* Function to print permutations of string
	This function takes three parameters:
	1. String
	2. Starting index of the string
	3. Ending index of the string. */
	public static void permute(ref StringBuilder a, int l, int r)
	{
		int i;
		if (l == r)
			Console.WriteLine(a[l]);
		else	
		{
			for (i = l; i <= r; i++)
			{
				swap(ref (a[l]), ref (a[i]));
				permute(ref a, l+1, r);
				swap(ref (a[l]), ref (a[i])); //backtrack
			}
		}
	}

	/* Driver program to test above functions */
	public static void Main()
	{
		StringBuilder str = new StringBuilder("ABC");
		int n = strlen(ref str);
		permute(str, 0, n-1);
	}
}
using System;
class Test{
	static Test(){
		Console.WriteLine("Inside static constructor");
	}
	public Test(int a)
	{
		Console.WriteLine(a);
	}
	public static void Print(){
		Console.WriteLine("Inside Print");
	}
}
class StaticConstructor{
	public static void Main(){
		Console.WriteLine("Inside Main");
		Test test0=new Test(10);
		//Test test=new Test();//Test does not contain a constructor that takes 0 arguments
		Test.Print();
		//Console.WriteLine(test.GetHashCode());
		//Test test1=new Test();
		//test1.Print();
		//Console.WriteLine(test1.GetHashCode());
	}
}
using System;
using System.Text;

class BuilderString
{
	public static void Main()
	{
		bool found = true;
		Console.WriteLine("Enter a string: ");
		string s = Console.ReadLine();
		string temp = string.Empty;
		temp=s[0].ToString();
		for(int i=1;i<s.Length;i++)
		{
			found = false;
			for(int j=0;j<temp.Length;j++)
			{
				if(s[i]==temp[j])
					found=true;
			}
			if(!found)
				temp+=s[i].ToString();
		}
		StringBuilder str = new StringBuilder(temp);
		//Console.WriteLine(n+"  "+str[0]);
		permute(str, 0, str.Length-1);
		//Console.WriteLine(str[0]+"  "+str[1]);
	}
	
	public static void permute(StringBuilder sb, int l, int r)
	{
		char x=' ', y=' ';
		if(l==r)
			Console.WriteLine(sb);
		else
		{
			for(int i=l;i<=r;i++)
			{
				x = sb[l];
				y = sb[i];
				swap(ref x,ref y);
				sb[l] = x;
				sb[i] = y;
				permute(sb, l+1, r);
				x = sb[l];
				y = sb[i];
				swap(ref x,ref y);
				sb[l] = x;
				sb[i] = y;
			
			}
		}
		
	}
	
	public static void swap(ref char a,ref char b)
	{
		char temp;
		temp=a;
		a=b;
		b=temp;
	}
	
}
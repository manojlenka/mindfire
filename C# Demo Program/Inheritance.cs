using System;
public class Base{//if public is removed:-Inconsistent accessibility:Base class 'Base' is less accessible to child class 'Child'
	public int a=2,b=3;
	public Base(){
		Console.WriteLine("Inside Base");
	}
	/*public int A{
		get{
			return a;
		}
		set{
			a=value;
		}
	}
	public int B{
		get{
			return b;
		}
		set{
			b=value;
		}
	}*/
}

public class Child: Base{
	public Child(){
		Console.WriteLine("Inside Child class");
	}
	public void Add(){
		Console.WriteLine(a+b);
	}
	//public A(){}//Method must have return Type
}

public class Inheritance{
	public static void Main(){
		Console.WriteLine("Inside Main");
		Child child=new Child();
		child.Add();
	}
}
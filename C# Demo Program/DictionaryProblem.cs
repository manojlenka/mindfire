using System;
using System.Collections.Generic;

class DictionaryProblem
{
	public static void Main()
	{
		bool found = true;
		string choice ="n";
		string pro = string.Empty;
		string cat = string.Empty;
		int amount = 0;
		int raised = 0;
		Dictionary<string,string> details = new Dictionary<string,string>();
		do{
			Console.WriteLine("Enter product name: ");
			pro = Console.ReadLine().Trim();
			Console.WriteLine("Enter product category: ");
			cat = Console.ReadLine().Trim();
			details.Add(pro,cat);
			Console.WriteLine("Want to add more product (y/n): ");
			choice = Console.ReadLine().Trim().ToLower();
		}while(choice=="y");
		
		Dictionary<string,int> priceDetails = new Dictionary<string,int>();
		
		ICollection<string> c = details.Keys;
		
		foreach(string a in c)
		{
			Console.WriteLine("Enter {0} amount: ",a);
			Int32.TryParse(Console.ReadLine().Trim(),out amount);
			priceDetails.Add(a,amount);
		}
		
		ICollection<string> d = details.Keys;
		
		Console.WriteLine("Enter amount to be raised for the product: ");
		Int32.TryParse(Console.ReadLine().Trim(),out raised);
		Console.WriteLine("Enter category to be raised amount: ");
		cat = Console.ReadLine();
		
		foreach(string a in c)
		{
			if(cat.ToLower().Equals(details[a].ToString().ToLower()))
			{
				found = true;
				foreach(string b in d)
				{
					if(a.ToLower().Equals(b.ToLower()))
					{
						priceDetails[b] = priceDetails[b]+raised;
					}
				}
			}
		}
		if(!found)
			Console.WriteLine("Category does not exist.");
		else{
			Console.WriteLine("Product : Price");
			foreach(string b in d)
				{
					Console.WriteLine(b+"     "+priceDetails[b]);
				}
		}
		Console.Read();
	}
}
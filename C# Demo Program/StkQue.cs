using System;
class StackOperation
{
	int i=0;
	string[] st=new string[1000];
	public void insertion(string cho)
	{
		if(i<1000)
		{
			while(cho=="y")
			{
				Console.WriteLine("Enter string to implement stack");
				st[i]=Console.ReadLine();
				i++;
				Console.WriteLine("Want to PUSH more data in stack:(y/n):");
				cho=Console.ReadLine().ToLower();
			}
		}
		else{
			Console.WriteLine("Stack Overflow");
		}
	}
	
	public void deletion(string cho)
	{
		if(i>1)
		{
			while(cho=="y")
			{
				i--;
				Console.WriteLine("Want to POP more data in stack:(y/n):");
				cho=Console.ReadLine().ToLower();
			}
		}
		else{
			Console.WriteLine("Stack underflow");
		}
	}
	
	public void show()
	{
		for(int j=i-1;j>=0;j--)
		{
			Console.WriteLine(st[j]);
		}
	}
}

class QueueOperation
{
	int i=0,start=0;
	string[] st=new string[1000];
	public void insertion(string cho)
	{
		if(i<1000)
		{
			while(cho=="y")
			{
				Console.WriteLine("Enter string to implement queue");
				st[i]=Console.ReadLine();
				i++;
				Console.WriteLine("Want to insert more data in queue:(y/n):");
				cho=Console.ReadLine().ToLower();
			}
		}
		else{
			Console.WriteLine("Queue Overflow");
		}
	}
	
	public void deletion(string cho)
	{
		if(start<i)
		{
			while(cho=="y")
			{
				start++;
				Console.WriteLine("Want to delete more data in queue:(y/n):");
				cho=Console.ReadLine().ToLower();
				if(start>=i)
				{
					Console.WriteLine("Queue underflow");
					break;
				}
			}
		}
		else{
			Console.WriteLine("Queue underflow");
		}
	}
	
	public void show()
	{
		for(int j=start;j<i;j++)
		{
			Console.WriteLine(st[j]);
		}
	}
}

class StkQue
{
	public static void Main(){
		Console.WriteLine("Want stack operation(y/n): ");
		string flag=Console.ReadLine().ToLower();
		if(flag=="y")
		{
			stOp();
		}
		Console.WriteLine("Want queue operation(y/n): ");
		flag=Console.ReadLine().ToLower();
		if(flag=="y")
		{
			quOp();
		}
		Console.Read();
	}
	
	public static void stOp()
	{
		StackOperation obj=new StackOperation();
		Console.WriteLine("Want to PUSH data in stack:(y/n):");
		string choise=Console.ReadLine().ToLower();
		if(choise=="y")
		{
			obj.insertion(choise);
		}
		
		Console.WriteLine("After stack insertion: ");
		obj.show();
		
		Console.WriteLine("Want to POP data in stack:(y/n):");
		choise=Console.ReadLine().ToLower();
		if(choise=="y")
		{
			obj.deletion(choise);
		}
		Console.WriteLine("After stack deletion: ");
		obj.show();
	}
	
	public static void quOp()
	{
		QueueOperation obj=new QueueOperation();
		Console.WriteLine("Want to insert data in queue:(y/n):");
		string choise=Console.ReadLine().ToLower();
		if(choise=="y")
		{
			obj.insertion(choise);
		}
		
		Console.WriteLine("After queue insertion: ");
		obj.show();
		
		Console.WriteLine("Want to delete data from Queue:(y/n):");
		choise=Console.ReadLine().ToLower();
		if(choise=="y")
		{
			obj.deletion(choise);
		}
		Console.WriteLine("After queue deletion: ");
		obj.show();
	}
}
using System;
class A
{
	public static void Main(){
		int i,j,temp,m,n,count=0;
		Int32.TryParse(Console.ReadLine(), out n);
		string str;
		str=Console.ReadLine();
		int[] a = Array.ConvertAll<string,int>(str.Split(' '),int.Parse);
		for(i=0;i<a.Length;i++)
		{
			count=0;
			for(j=i;j<a.Length;j++)
			{
				if(a[i]>a[j])
				{
					m=a[i];
					n=a[j];
					while(m%n!=0)
					{
						temp=m%n;
						m=n;
						n=temp;
					}
					if(n==1)
					{
						count++;
					}
				}
			}
		}
		Console.WriteLine(count);
	}
}
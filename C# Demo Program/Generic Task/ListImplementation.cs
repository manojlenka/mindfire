using System;
using System.Collections.Generic;
public class Customer
{
	private int customerId,age;
	private string firstName, lastName, address;
	public int CustomerId
	{
		get{
			return customerId;
		}
		set{
			customerId = value;
		}
	}
	public int Age
	{
		get{
			return age;
		}
		set{
			age = value;
		}
	}
	public string FirstName
	{
		get{
			return firstName;
		}
		set{
			firstName = value;
		}
	}
	public string LastName
	{
		get{
			return lastName;
		}
		set{
			lastName = value;
		}
	}
	public string Address
	{
		get{
			return address;
		}
		set{
			address = value;
		}
	}
	
	public int CompareTo(Customer rhs, CustomerComparer.ComparisonType which)
    {
        switch (which)
        {
            case CustomerComparer.ComparisonType.CustomerId:
                return this.CustomerId.CompareTo(rhs.CustomerId);
            case CustomerComparer.ComparisonType.Age:
                return this.Age.CompareTo(rhs.Age);
			case CustomerComparer.ComparisonType.FirstName:
                return this.FirstName.CompareTo(rhs.FirstName);
			case CustomerComparer.ComparisonType.Address:
                return this.Address.CompareTo(rhs.Address);
			case CustomerComparer.ComparisonType.LastName:
                return this.LastName.CompareTo(rhs.LastName);
        }
        return 0;
    }
	
	public static CustomerComparer GetComparer()
    {
        return new CustomerComparer();
    }
}

class ListImplementation
{
	public static void Main()
	{
		List<Customer> obj2 = new List<Customer>();
		ShowMenu(obj2);
		Console.Read();
	}
	
	public static void ShowMenu(List<Customer> obj)
	{
		Console.WriteLine("1: Add Cusomer Details");
		Console.WriteLine("2: Remove Cusomer Details");
		Console.WriteLine("3: Search Cusomer Details");
		Console.WriteLine("4: Listing Cusomer Details");
		Console.WriteLine("5: Sorting Cusomer Details");
		string choise = Console.ReadLine().Trim();
		switch(choise)
		{
			case "1":
				Customer data = Add();
				if(data.Age!=0)
					obj.Add(data);
				else
					Console.WriteLine("Insufficient data to be added.");
				ShowMenu(obj);
				break;
			case "2":Remove(ref obj);
				ShowMenu(obj);
				break;
			case "3":Search(obj);
				ShowMenu(obj);
				break;
			case "4":Listing(obj);
				ShowMenu(obj);
				break;
			case "5":Sorting(ref obj);
				ShowMenu(obj);
				break;
			default:Console.WriteLine("Invalid Input");
				break;
		}
	}
	public static Customer Add()
	{
		Customer obj1 = new Customer();
		int id,age;
		Console.WriteLine("Enter Id of the Customer: ");
		bool a=Int32.TryParse(Console.ReadLine(),out id);
		if(a)
			obj1.CustomerId=id;
		else
		{
			Console.WriteLine("Invalid ID");
			return obj1;
		}
		Console.WriteLine("Enter First Name of the Customer: ");
		string fname = Console.ReadLine();
		obj1.FirstName=fname;
		Console.WriteLine("Enter Last Name of the Customer: ");
		string lname = Console.ReadLine();
		obj1.LastName=lname;
		Console.WriteLine("Enter Age of the Customer: ");
		bool b = Int32.TryParse(Console.ReadLine(),out age);
		if(b)
			obj1.Age=age;
		else
		{
			Console.WriteLine("Invalid Age");
			return obj1;
		}
		Console.WriteLine("Enter Address of the Customer: ");
		string address = Console.ReadLine();
		obj1.Address=address;
		return obj1;
	}
	
	public static void Remove(ref List<Customer> ob)
	{
		Console.WriteLine("1: Remove By ID");
		Console.WriteLine("2: Remove By First Name");
		Console.WriteLine("3: Remove By Last Name");
		Console.WriteLine("4: Remove By Age");
		Console.WriteLine("5: Remove By Address");
		Console.WriteLine("Enter Your Choice: ");
		string choice = Console.ReadLine();
		switch(choice)
		{
			case "1":RemoveById(ref ob);
				break;
			case "2":RemoveByFirstName(ref ob);
				break;
			case "3":RemoveByLastName(ref ob);
				break;
			case "4":RemoveByAge(ref ob);
				break;
			case "5":RemoveByAddress(ref ob);
				break;
			default:Console.WriteLine("Invalid Input");
				break;
		}
	}
	
	public static void RemoveById(ref List<Customer> ob)
	{
		int id=0,index=-1,flag=0;
		
		Console.WriteLine("Enter Id to be removed: ");
		bool a = Int32.TryParse(Console.ReadLine(),out id);
		if(a)
		{
			foreach(Customer cid in ob)
			{
				index++;
				if(cid.CustomerId==id)
				{
					ob.RemoveAt(index);
					flag=1;
					break;
				}
			}
			if(flag==0)
			{
				Console.WriteLine("Id not found");
			}
		}
		else
			Console.WriteLine("Please Enter a valid ID.");
		
	}
	
	public static void RemoveByFirstName(ref List<Customer> ob)
	{
		int index=-1,flag=0;
		Console.WriteLine("Enter First Name to be removed: ");
		string fname = Console.ReadLine().Trim();
			foreach(Customer cid in ob)
			{
				index++;
				if(fname.Equals(cid.FirstName))
				{
					ob.RemoveAt(index);
					flag=1;
					break;
				}
			}
			if(flag==0)
			{
				Console.WriteLine("First Name not found");
			}
	}
	
	public static void RemoveByLastName(ref List<Customer> ob)
	{
		int index=-1,flag=0;
		Console.WriteLine("Enter Last Name to be removed: ");
		string lname = Console.ReadLine().Trim();
			foreach(Customer cid in ob)
			{
				index++;
				if(lname.Equals(cid.LastName))
				{
					ob.RemoveAt(index);
					flag=1;
					break;
				}
			}
			if(flag==0)
			{
				Console.WriteLine("Last Name not found");
			}
	}
	
	public static void RemoveByAge(ref List<Customer> ob)
	{
		int id=0,index=-1,flag=0;
		Console.WriteLine("Enter Age to be removed: ");
		bool a = Int32.TryParse(Console.ReadLine(),out id);
		if(a)
		{
			foreach(Customer cid in ob)
			{
				index++;
				if(cid.Age==id)
				{
					ob.RemoveAt(index);
					flag=1;
					break;
				}
			}
			if(flag==0)
			{
				Console.WriteLine("Age not found");
			}
		}
		else
			Console.WriteLine("Please Enter a valid Age.");
	}
	
	public static void RemoveByAddress(ref List<Customer> ob)
	{
		int index=-1,flag=0;
		Console.WriteLine("Enter Address to be removed: ");
		string lname = Console.ReadLine().Trim();
			foreach(Customer cid in ob)
			{
				index++;
				if(lname.Equals(cid.Address))
				{
					ob.RemoveAt(index);
					flag=1;
					break;
				}
			}
			if(flag==0)
			{
				Console.WriteLine("Address not found");
			}
	}
	
	public static void Search(List<Customer> objPass)
	{
		Console.WriteLine("1: Search by Id");
		Console.WriteLine("2: Search by First Name");
		Console.WriteLine("3: Search by Last Name");
		Console.WriteLine("4: Search by Age");
		Console.WriteLine("5: Search by Address");
		Console.WriteLine("Search By: ");
		string ch = Console.ReadLine().Trim();
		switch(ch)
		{
			case "1":SearchById(objPass);
				break;
			case "2":SearchByFirstName(objPass);
				break;
			case "3":SearchByLastName(objPass);
				break;
			case "4":SearchByAge(objPass);
				break;
			case "5":SearchByAddress(objPass);
				break;
			default:Console.WriteLine("Invalid Choice");
				break;
		}
	}
	
	public static void SearchById(List<Customer> ob)
	{
		int id=0,index=-1,flag=0;
		Console.WriteLine("Enter Id to be searched: ");
		bool a = Int32.TryParse(Console.ReadLine(),out id);
		if(a)
		{
			foreach(Customer cid in ob)
			{
				index++;
				if(cid.CustomerId==id)
				{
					Console.WriteLine("Id found at index: "+index+1+"\n");
					Console.WriteLine("Customer Details: ");
					Console.WriteLine("Customer Id: "+ob[index].CustomerId);
					Console.WriteLine("Customer First Name: "+ob[index].FirstName);
					Console.WriteLine("Customer Last Name "+ob[index].LastName);
					Console.WriteLine("Customer Age: "+ob[index].Age);
					Console.WriteLine("Customer Address: "+ob[index].Address);
					Console.WriteLine("--------------------------------------------------------------------");
					flag=1;
				}
			}
			if(flag==0)
			{
				Console.WriteLine("Id not found");
			}
		}
		else
			Console.WriteLine("Please Enter a valid ID.");
	}

	public static void SearchByFirstName(List<Customer> ob)
	{
		int index=-1,flag=0;
		Console.WriteLine("Enter First Name to be searched: ");
		string fname = Console.ReadLine().Trim();
			foreach(Customer cid in ob)
			{
				index++;
				if(fname.Equals(cid.FirstName))
				{
					Console.WriteLine("First Name found at index: "+index+1+"\n");
					Console.WriteLine("Customer Details: ");
					Console.WriteLine("Customer Id: "+ob[index].CustomerId);
					Console.WriteLine("Customer First Name: "+ob[index].FirstName);
					Console.WriteLine("Customer Last Name "+ob[index].LastName);
					Console.WriteLine("Customer Age: "+ob[index].Age);
					Console.WriteLine("Customer Address: "+ob[index].Address);
					Console.WriteLine("--------------------------------------------------------------------");
					flag=1;
				}
			}
			if(flag==0)
			{
				Console.WriteLine("First Name not found");
			}
	}
	
	public static void SearchByLastName(List<Customer> ob)
	{
		int index=-1,flag=0;
		Console.WriteLine("Enter Last Name to be searched: ");
		string lname = Console.ReadLine().Trim();
			foreach(Customer cid in ob)
			{
				index++;
				if(lname.Equals(cid.LastName))
				{
					Console.WriteLine("Last Name found at index: "+index+1+"\n");
					Console.WriteLine("Customer Details: ");
					Console.WriteLine("Customer Id: "+ob[index].CustomerId);
					Console.WriteLine("Customer First Name: "+ob[index].FirstName);
					Console.WriteLine("Customer Last Name "+ob[index].LastName);
					Console.WriteLine("Customer Age: "+ob[index].Age);
					Console.WriteLine("Customer Address: "+ob[index].Address);
					Console.WriteLine("--------------------------------------------------------------------");
					flag=1;
				}
			}
			if(flag==0)
			{
				Console.WriteLine("Last Name not found");
			}
	}
	
	public static void SearchByAge(List<Customer> ob)
	{
		int id=0,index=-1,flag=0;
		Console.WriteLine("Enter Age to be searched: ");
		bool a = Int32.TryParse(Console.ReadLine(),out id);
		if(a)
		{
			foreach(Customer cid in ob)
			{
				index++;
				if(cid.Age==id)
				{
					Console.WriteLine("Age found at index: "+index+1+"\n");
					Console.WriteLine("Customer Details: ");
					Console.WriteLine("Customer Id: "+ob[index].CustomerId);
					Console.WriteLine("Customer First Name: "+ob[index].FirstName);
					Console.WriteLine("Customer Last Name "+ob[index].LastName);
					Console.WriteLine("Customer Age: "+ob[index].Age);
					Console.WriteLine("Customer Address: "+ob[index].Address);
					Console.WriteLine("--------------------------------------------------------------------");
					flag=1;
				}
			}
			if(flag==0)
			{
				Console.WriteLine("Age not found");
			}
		}
		else
			Console.WriteLine("Please Enter a valid Age.");
	}
	
	public static void SearchByAddress(List<Customer> ob)
	{
		int index=-1,flag=0;
		Console.WriteLine("Enter Address to be searched: ");
		string lname = Console.ReadLine().Trim();
			foreach(Customer cid in ob)
			{
				index++;
				if(lname.Equals(cid.Address))
				{
					Console.WriteLine("Address found at index: "+index+1+"\n");
					Console.WriteLine("Customer Details: ");
					Console.WriteLine("Customer Id: "+ob[index].CustomerId);
					Console.WriteLine("Customer First Name: "+ob[index].FirstName);
					Console.WriteLine("Customer Last Name "+ob[index].LastName);
					Console.WriteLine("Customer Age: "+ob[index].Age);
					Console.WriteLine("Customer Address: "+ob[index].Address);
					Console.WriteLine("--------------------------------------------------------------------");
					flag=1;
				}
			}
			if(flag==0)
			{
				Console.WriteLine("Address not found");
			}
	}
	
	public static void Listing(List<Customer> l)
	{
		Console.WriteLine("Now data are: ");
		Console.WriteLine("--------------------------------------------------------------------");
		foreach(Customer  details in l)
		{
			Console.WriteLine("Customer Id: "+details.CustomerId);
			Console.WriteLine("Customer First Name: "+details.FirstName);
			Console.WriteLine("Customer Last Name "+details.LastName);
			Console.WriteLine("Customer Age: "+details.Age);
			Console.WriteLine("Customer Address: "+details.Address);
			Console.WriteLine("--------------------------------------------------------------------");
		}
	}
	public static void Sorting(ref List<Customer> ob)
	{
		Console.WriteLine("1: Sort By Id");
		Console.WriteLine("2: Sort By First Name");
		Console.WriteLine("3: Sort By Last Name");
		Console.WriteLine("4: Sort By Age");
		Console.WriteLine("5: Sort By Address");
		Console.WriteLine("Enter your choice: ");
		string choice = Console.ReadLine();
		switch(choice)
		{
			case "1":sortById(ref ob);
				break;
			case "2":sortByFirstName(ref ob);
				break;
			case "3":sortByLastName(ref ob);
				break;
			case "4":sortByAge(ref ob);
				break;
			case "5":sortByAddress(ref ob);
				break;
			default:Console.WriteLine("Invalid Choice.");
				break;
		}
	}
	
	public static void sortById(ref List<Customer> ob)
	{
		Console.WriteLine("\nSort Based on ID: ");
		CustomerComparer c = Customer.GetComparer();
        c.WhichComparison = CustomerComparer.ComparisonType.CustomerId;
        ob.Sort(c);
        foreach (Customer cus in ob)
        {
            Console.WriteLine("Customer Id: "+cus.CustomerId);
			Console.WriteLine("Customer First Name: "+cus.FirstName);
			Console.WriteLine("Customer Last Name "+cus.LastName);
			Console.WriteLine("Customer Age: "+cus.Age);
			Console.WriteLine("Customer Address: "+cus.Address);
			Console.WriteLine("--------------------------------------------------------------------");
        }
	}
	
	public static void sortByFirstName(ref List<Customer> ob)
	{
		Console.WriteLine("\nSort Based on First Name: ");
		CustomerComparer c = Customer.GetComparer();
        c.WhichComparison = CustomerComparer.ComparisonType.FirstName;
        ob.Sort(c);
        foreach (Customer cus in ob)
        {
            Console.WriteLine("Customer Id: "+cus.CustomerId);
			Console.WriteLine("Customer First Name: "+cus.FirstName);
			Console.WriteLine("Customer Last Name "+cus.LastName);
			Console.WriteLine("Customer Age: "+cus.Age);
			Console.WriteLine("Customer Address: "+cus.Address);
			Console.WriteLine("--------------------------------------------------------------------");
        }
	}
	
	public static void sortByLastName(ref List<Customer> ob)
	{
		Console.WriteLine("\nSort Based on Last Name: ");
		CustomerComparer c = Customer.GetComparer();
        c.WhichComparison = CustomerComparer.ComparisonType.LastName;
        ob.Sort(c);
        foreach (Customer cus in ob)
        {
            Console.WriteLine("Customer Id: "+cus.CustomerId);
			Console.WriteLine("Customer First Name: "+cus.FirstName);
			Console.WriteLine("Customer Last Name "+cus.LastName);
			Console.WriteLine("Customer Age: "+cus.Age);
			Console.WriteLine("Customer Address: "+cus.Address);
			Console.WriteLine("--------------------------------------------------------------------");
        }
	}
	
	public static void sortByAge(ref List<Customer> ob)
	{
		Console.WriteLine("\nSort Based on Age: ");
		CustomerComparer c = Customer.GetComparer();
        c.WhichComparison = CustomerComparer.ComparisonType.Age;
        ob.Sort(c);
        foreach (Customer cus in ob)
        {
            Console.WriteLine("Customer Id: "+cus.CustomerId);
			Console.WriteLine("Customer First Name: "+cus.FirstName);
			Console.WriteLine("Customer Last Name "+cus.LastName);
			Console.WriteLine("Customer Age: "+cus.Age);
			Console.WriteLine("Customer Address: "+cus.Address);
			Console.WriteLine("--------------------------------------------------------------------");
        }
	}
	
	public static void sortByAddress(ref List<Customer> ob)
	{
		Console.WriteLine("\nSort Based on Address: ");
		CustomerComparer c = Customer.GetComparer();
        c.WhichComparison = CustomerComparer.ComparisonType.Address;
        ob.Sort(c);
        foreach (Customer cus in ob)
        {
            Console.WriteLine("Customer Id: "+cus.CustomerId);
			Console.WriteLine("Customer First Name: "+cus.FirstName);
			Console.WriteLine("Customer Last Name "+cus.LastName);
			Console.WriteLine("Customer Age: "+cus.Age);
			Console.WriteLine("Customer Address: "+cus.Address);
			Console.WriteLine("--------------------------------------------------------------------");
        }
	}
	
	
}


	public class CustomerComparer : IComparer<Customer>
    {
        private ComparisonType whichComparison;
        public enum ComparisonType
        {
            CustomerId,
            Age,
			FirstName,
			LastName,
			Address
        };

        public bool Equals(Customer lhs, Customer rhs)
        {
            return this.Compare(lhs, rhs) == 0;
        }
   
        public int Compare(Customer lhs, Customer rhs)
        {
            return lhs.CompareTo(rhs, WhichComparison);
        }

        public ComparisonType WhichComparison
        {
            get { return whichComparison; }
            set { whichComparison = value; }
        }
    }

/*
1.CustomerId
2.FirstName
3.LastName
4.Age
5.Address
*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PublisherClass;

public class Customer : IComparable
{
	private int customerId,age;
	private string firstName, lastName, address;
	public static int ComparisonType=0;
	public int CustomerId
	{
		get{
			return customerId;
		}
		set{
			customerId = value;
		}
	}
	public int Age
	{
		get{
			return age;
		}
		set{
			age = value;
		}
	}
	public string FirstName
	{
		get{
			return firstName;
		}
		set{
			firstName = value;
		}
	}
	public string LastName
	{
		get{
			return lastName;
		}
		set{
			lastName = value;
		}
	}
	public string Address
	{
		get{
			return address;
		}
		set{
			address = value;
		}
	}
	
	public int CompareTo(Object obj)
        {
			Customer rhs = obj as Customer;
            switch (ComparisonType)
            {
                case 1:
                    return this.CustomerId.CompareTo(rhs.CustomerId);
                case 2:
                    return this.Age.CompareTo(rhs.Age);
				case 3:
                    return this.FirstName.CompareTo(rhs.FirstName);
				case 4:
                    return this.LastName.CompareTo(rhs.LastName);
				case 5:
					return this.Address.CompareTo(rhs.Address);
            }
            return 0;
        }
}
/*
delegate void EventGeneration(int count);

class Publisher
{
	public static int count = 0;
	public event EventGeneration addEvent;
	
	public void showEvent()
	{
		if(addEvent!=null)
		{
			count++;
			addEvent(count);
		}
	}
	
	public void removeEvent()
	{
		if(addEvent!=null)
		{
			if(count>0)
			{
				count--;
				addEvent(count);
			}
		}
	}
}
*/
public class CustomerClass
{
	public static void Main()
	{
		CustomerObjectCollection<Customer> obj2 = new CustomerObjectCollection<Customer>();
		ShowMenu(obj2);
		Console.Read();
	}
	
	public static void EventFired(int count)
	{
		Console.WriteLine("Event Fired! ------ Count :  "+count);
	}
	
	public static void ShowMenu(CustomerObjectCollection<Customer> obj)
	{
		Console.WriteLine("1: Add Cusomer Details");
		Console.WriteLine("2: Remove Cusomer Details");
		Console.WriteLine("3: Search Cusomer Details");
		Console.WriteLine("4: Listing Cusomer Details");
		Console.WriteLine("5: Sorting Cusomer Details");
		string choise = Console.ReadLine().Trim();
		Publisher pub = new Publisher();
		switch(choise)
		{
			case "1":
				Customer data = Add();
				if(data.Age!=0)
				{
					obj.Add(data);
					pub.addEvent += EventFired;
					pub.showEvent();
				}
				else
					Console.WriteLine("Insufficient data to be added.");
				ShowMenu(obj);
				break;
				case "2":Remove(ref obj);
				pub.addEvent += EventFired;
				pub.removeEvent();
				ShowMenu(obj);
				break;
			case "3":Search(obj);
				ShowMenu(obj);
				break;
			case "4":Listing(obj);
				ShowMenu(obj);
				break;
			case "5":Sorting(ref obj);
				ShowMenu(obj);
				break;
			default:Console.WriteLine("Invalid Input");
				break;
		}
	}
	
	public static Customer Add()
	{
		Customer obj1 = new Customer();
		int id,age;
		Console.WriteLine("Enter Id of the Customer: ");
		bool a=Int32.TryParse(Console.ReadLine(),out id);
		if(a)
			obj1.CustomerId=id;
		else
		{
			Console.WriteLine("Invalid ID");
			return obj1;
		}
		Console.WriteLine("Enter First Name of the Customer: ");
		string fname = Console.ReadLine();
		obj1.FirstName=fname;
		Console.WriteLine("Enter Last Name of the Customer: ");
		string lname = Console.ReadLine();
		obj1.LastName=lname;
		Console.WriteLine("Enter Age of the Customer: ");
		bool b = Int32.TryParse(Console.ReadLine(),out age);
		if(b)
			obj1.Age=age;
		else
		{
			Console.WriteLine("Invalid Age");
			return obj1;
		}
		Console.WriteLine("Enter Address of the Customer: ");
		string address = Console.ReadLine();
		obj1.Address=address;
		return obj1;
	}
	
	public static void Remove(ref CustomerObjectCollection<Customer> ob)
	{
		Console.WriteLine("1: Remove By ID");
		Console.WriteLine("2: Remove By First Name");
		Console.WriteLine("3: Remove By Last Name");
		Console.WriteLine("4: Remove By Age");
		Console.WriteLine("5: Remove By Address");
		Console.WriteLine("Enter Your Choice: ");
		string choice = Console.ReadLine();
		switch(choice)
		{
			case "1":RemoveById(ref ob);
				break;
			case "2":RemoveByFirstName(ref ob);
				break;
			case "3":RemoveByLastName(ref ob);
				break;
			case "4":RemoveByAge(ref ob);
				break;
			case "5":RemoveByAddress(ref ob);
				break;
			default:Console.WriteLine("Invalid Input");
				break;
		}
	}
	
	public static void RemoveById(ref CustomerObjectCollection<Customer> ob)
	{
		int id=0,index=-1,flag=0;
		
		Console.WriteLine("Enter Id to be removed: ");
		bool a = Int32.TryParse(Console.ReadLine(),out id);
		if(a)
		{
			foreach(Customer cid in ob)
			{
				index++;
				if(cid.CustomerId==id)
				{
					ob.Remove(ob[index]);
					flag=1;
					break;
				}
			}
			if(flag==0)
			{
				Console.WriteLine("Id not found");
			}
		}
		else
			Console.WriteLine("Please Enter a valid ID.");
		
	}
	
	public static void RemoveByFirstName(ref CustomerObjectCollection<Customer> ob)
	{
		int index=-1,flag=0;
		Console.WriteLine("Enter First Name to be removed: ");
		string fname = Console.ReadLine().Trim();
			foreach(Customer cid in ob)
			{
				index++;
				if(fname.Equals(cid.FirstName))
				{
					ob.Remove(ob[index]);
					flag=1;
					break;
				}
			}
			if(flag==0)
			{
				Console.WriteLine("First Name not found");
			}
	}
	
	public static void RemoveByLastName(ref CustomerObjectCollection<Customer> ob)
	{
		int index=-1,flag=0;
		Console.WriteLine("Enter Last Name to be removed: ");
		string lname = Console.ReadLine().Trim();
		foreach(Customer cid in ob)
		{
			index++;
			if(lname.Equals(cid.LastName))
			{
				ob.Remove(ob[index]);
				flag=1;
				break;
			}
		}
		if(flag==0)
		{
			Console.WriteLine("Last Name not found");
		}
	}
	
	public static void RemoveByAge(ref CustomerObjectCollection<Customer> ob)
	{
		int id=0,index=-1,flag=0;
		Console.WriteLine("Enter Age to be removed: ");
		bool a = Int32.TryParse(Console.ReadLine(),out id);
		if(a)
		{
			foreach(Customer cid in ob)
			{
				index++;
				if(cid.Age==id)
				{
					ob.Remove(ob[index]);
					flag=1;
					break;
				}
			}
			if(flag==0)
			{
				Console.WriteLine("Age not found");
			}
		}
		else
			Console.WriteLine("Please Enter a valid Age.");
	}
	
	public static void RemoveByAddress(ref CustomerObjectCollection<Customer> ob)
	{
		int index=-1,flag=0;
		Console.WriteLine("Enter Address to be removed: ");
		string lname = Console.ReadLine().Trim();
		foreach(Customer cid in ob)
		{
			index++;
			if(lname.Equals(cid.Address))
			{
				ob.Remove(ob[index]);
				flag=1;
				break;
			}
		}
		if(flag==0)
		{
			Console.WriteLine("Address not found");
		}
	}
	
	public static void Search(CustomerObjectCollection<Customer> objPass)
	{
		Console.WriteLine("1: Search by Id");
		Console.WriteLine("2: Search by First Name");
		Console.WriteLine("3: Search by Last Name");
		Console.WriteLine("4: Search by Age");
		Console.WriteLine("5: Search by Address");
		Console.WriteLine("Search By: ");
		string ch = Console.ReadLine().Trim();
		switch(ch)
		{
			case "1":SearchById(objPass);
				break;
			case "2":SearchByFirstName(objPass);
				break;
			case "3":SearchByLastName(objPass);
				break;
			case "4":SearchByAge(objPass);
				break;
			case "5":SearchByAddress(objPass);
				break;
			default:Console.WriteLine("Invalid Choice");
				break;
		}
	}
	
	public static void SearchById(CustomerObjectCollection<Customer> ob)
	{
		int id=0,index=-1,flag=0;
		Console.WriteLine("Enter Id to be searched: ");
		bool a = Int32.TryParse(Console.ReadLine(),out id);
		if(a)
		{
			foreach(Customer cid in ob)
			{
				index++;
				if(cid.CustomerId==id)
				{
					Console.WriteLine("Id found at index: "+index+1+"\n");
					Console.WriteLine("Customer Details: ");
					Console.WriteLine("Customer Id: "+ob[index].CustomerId);
					Console.WriteLine("Customer First Name: "+ob[index].FirstName);
					Console.WriteLine("Customer Last Name "+ob[index].LastName);
					Console.WriteLine("Customer Age: "+ob[index].Age);
					Console.WriteLine("Customer Address: "+ob[index].Address);
					Console.WriteLine("--------------------------------------------------------------------");
					flag=1;
				}
			}
			if(flag==0)
			{
				Console.WriteLine("Id not found");
			}
		}
		else
			Console.WriteLine("Please Enter a valid ID.");
	}

	public static void SearchByFirstName(CustomerObjectCollection<Customer> ob)
	{
		int index=-1,flag=0;
		Console.WriteLine("Enter First Name to be searched: ");
		string fname = Console.ReadLine().Trim();
		foreach(Customer cid in ob)
		{
			index++;
			if(fname.Equals(cid.FirstName))
			{
				Console.WriteLine("First Name found at index: "+index+1+"\n");
				Console.WriteLine("Customer Details: ");
				Console.WriteLine("Customer Id: "+ob[index].CustomerId);
				Console.WriteLine("Customer First Name: "+ob[index].FirstName);
				Console.WriteLine("Customer Last Name "+ob[index].LastName);
				Console.WriteLine("Customer Age: "+ob[index].Age);
				Console.WriteLine("Customer Address: "+ob[index].Address);
				Console.WriteLine("--------------------------------------------------------------------");
				flag=1;
			}
		}
		if(flag==0)
		{
			Console.WriteLine("First Name not found");
		}
	}
	
	public static void SearchByLastName(CustomerObjectCollection<Customer> ob)
	{
		int index=-1,flag=0;
		Console.WriteLine("Enter Last Name to be searched: ");
		string lname = Console.ReadLine().Trim();
		foreach(Customer cid in ob)
		{
			index++;
			if(lname.Equals(cid.LastName))
			{
				Console.WriteLine("Last Name found at index: "+index+1+"\n");
				Console.WriteLine("Customer Details: ");
				Console.WriteLine("Customer Id: "+ob[index].CustomerId);
				Console.WriteLine("Customer First Name: "+ob[index].FirstName);
				Console.WriteLine("Customer Last Name "+ob[index].LastName);
				Console.WriteLine("Customer Age: "+ob[index].Age);
				Console.WriteLine("Customer Address: "+ob[index].Address);
				Console.WriteLine("--------------------------------------------------------------------");
				flag=1;
			}
		}
		if(flag==0)
		{
			Console.WriteLine("Last Name not found");
		}
	}
	
	public static void SearchByAge(CustomerObjectCollection<Customer> ob)
	{
		int id=0,index=-1,flag=0;
		Console.WriteLine("Enter Age to be searched: ");
		bool a = Int32.TryParse(Console.ReadLine(),out id);
		if(a)
		{
			foreach(Customer cid in ob)
			{
				index++;
				if(cid.Age==id)
				{
					Console.WriteLine("Age found at index: "+index+1+"\n");
					Console.WriteLine("Customer Details: ");
					Console.WriteLine("Customer Id: "+ob[index].CustomerId);
					Console.WriteLine("Customer First Name: "+ob[index].FirstName);
					Console.WriteLine("Customer Last Name "+ob[index].LastName);
					Console.WriteLine("Customer Age: "+ob[index].Age);
					Console.WriteLine("Customer Address: "+ob[index].Address);
					Console.WriteLine("--------------------------------------------------------------------");
					flag=1;
				}
			}
			if(flag==0)
			{
				Console.WriteLine("Age not found");
			}
		}
		else
			Console.WriteLine("Please Enter a valid Age.");
	}
	
	public static void SearchByAddress(CustomerObjectCollection<Customer> ob)
	{
		int index=-1,flag=0;
		Console.WriteLine("Enter Address to be searched: ");
		string lname = Console.ReadLine().Trim();
		foreach(Customer cid in ob)
		{
			index++;
			if(lname.Equals(cid.Address))
			{
				Console.WriteLine("Address found at index: "+index+1+"\n");
				Console.WriteLine("Customer Details: ");
				Console.WriteLine("Customer Id: "+ob[index].CustomerId);
				Console.WriteLine("Customer First Name: "+ob[index].FirstName);
				Console.WriteLine("Customer Last Name "+ob[index].LastName);
				Console.WriteLine("Customer Age: "+ob[index].Age);
				Console.WriteLine("Customer Address: "+ob[index].Address);
				Console.WriteLine("--------------------------------------------------------------------");
				flag=1;
			}
		}
		if(flag==0)
		{
			Console.WriteLine("Address not found");
		}
	}
	
	public static void Listing(CustomerObjectCollection<Customer> l)
	{
		Console.WriteLine("Now data are: ");
		Console.WriteLine("--------------------------------------------------------------------");
		foreach(Customer  details in l)
		{
			Console.WriteLine("Customer Id: "+details.CustomerId);
			Console.WriteLine("Customer First Name: "+details.FirstName);
			Console.WriteLine("Customer Last Name "+details.LastName);
			Console.WriteLine("Customer Age: "+details.Age);
			Console.WriteLine("Customer Address: "+details.Address);
			Console.WriteLine("--------------------------------------------------------------------");
		}
	}
	
	public static void Sorting(ref CustomerObjectCollection<Customer> ob)
	{
		Console.WriteLine("1: Sort By Id");
		Console.WriteLine("2: Sort By First Name");
		Console.WriteLine("3: Sort By Last Name");
		Console.WriteLine("4: Sort By Age");
		Console.WriteLine("5: Sort By Address");
		Console.WriteLine("Enter your choice: ");
		string choice = Console.ReadLine();
		switch(choice)
		{
			case "1":sortById(ref ob);
				break;
			case "2":sortByFirstName(ref ob);
				break;
			case "3":sortByLastName(ref ob);
				break;
			case "4":sortByAge(ref ob);
				break;
			case "5":sortByAddress(ref ob);
				break;
			default:Console.WriteLine("Invalid Choice.");
				break;
		}
	}
	
	public static void sortById(ref CustomerObjectCollection<Customer> ob)
	{
		Console.WriteLine("\nSort Based on ID: ");
		Customer.ComparisonType = 1;
        ob.Sort();
        foreach (Customer cus in ob)
        {
            Console.WriteLine("Customer Id: "+cus.CustomerId);
			Console.WriteLine("Customer First Name: "+cus.FirstName);
			Console.WriteLine("Customer Last Name "+cus.LastName);
			Console.WriteLine("Customer Age: "+cus.Age);
			Console.WriteLine("Customer Address: "+cus.Address);
			Console.WriteLine("--------------------------------------------------------------------");
        }
	}
	
	public static void sortByFirstName(ref CustomerObjectCollection<Customer> ob)
	{
		Console.WriteLine("\nSort Based on First Name: ");
		Customer.ComparisonType = 3;
        ob.Sort();
        foreach (Customer cus in ob)
        {
            Console.WriteLine("Customer Id: "+cus.CustomerId);
			Console.WriteLine("Customer First Name: "+cus.FirstName);
			Console.WriteLine("Customer Last Name "+cus.LastName);
			Console.WriteLine("Customer Age: "+cus.Age);
			Console.WriteLine("Customer Address: "+cus.Address);
			Console.WriteLine("--------------------------------------------------------------------");
        }
	}
	
	public static void sortByLastName(ref CustomerObjectCollection<Customer> ob)
	{
		Console.WriteLine("\nSort Based on Last Name: ");
		Customer.ComparisonType = 4;
        ob.Sort();
        foreach (Customer cus in ob)
        {
            Console.WriteLine("Customer Id: "+cus.CustomerId);
			Console.WriteLine("Customer First Name: "+cus.FirstName);
			Console.WriteLine("Customer Last Name "+cus.LastName);
			Console.WriteLine("Customer Age: "+cus.Age);
			Console.WriteLine("Customer Address: "+cus.Address);
			Console.WriteLine("--------------------------------------------------------------------");
        }
	}
	
	public static void sortByAge(ref CustomerObjectCollection<Customer> ob)
	{
		Console.WriteLine("\nSort Based on Age: ");
		Customer.ComparisonType = 2;
        ob.Sort();
        foreach (Customer cus in ob)
        {
            Console.WriteLine("Customer Id: "+cus.CustomerId);
			Console.WriteLine("Customer First Name: "+cus.FirstName);
			Console.WriteLine("Customer Last Name "+cus.LastName);
			Console.WriteLine("Customer Age: "+cus.Age);
			Console.WriteLine("Customer Address: "+cus.Address);
			Console.WriteLine("--------------------------------------------------------------------");
        }
	}
	
	public static void sortByAddress(ref CustomerObjectCollection<Customer> ob)
	{
		Console.WriteLine("\nSort Based on Address: ");
		Customer.ComparisonType = 5;
        ob.Sort();
        foreach (Customer cus in ob)
        {
            Console.WriteLine("Customer Id: "+cus.CustomerId);
			Console.WriteLine("Customer First Name: "+cus.FirstName);
			Console.WriteLine("Customer Last Name "+cus.LastName);
			Console.WriteLine("Customer Age: "+cus.Age);
			Console.WriteLine("Customer Address: "+cus.Address);
			Console.WriteLine("--------------------------------------------------------------------");
        }
	}	

}

	public class CustomerObjectCollection<T> : ICollection<T> where T : Customer
    {
        //inner ArrayList object
        protected ArrayList _innerArray;
        //flag for setting collection to read-only
        //mode (not used in this example)
        protected bool _IsReadOnly;

        // Default constructor
        public CustomerObjectCollection()
        {
            _innerArray = new ArrayList();
        }

        // Default accessor for the collection 
        public T this[int index]
        {
            get
            {
                return (T)_innerArray[index];
            }
            set
            {
                _innerArray[index] = value;
            }
        }

        // Number of elements in the collection
        public virtual int Count
        {
            get
            {
                return _innerArray.Count;
            }
        }

        // Flag sets whether or not this collection is read-only
        public virtual bool IsReadOnly
        {
            get
            {
                return _IsReadOnly;
            }
        }

        // Add a Customer object to the collection
        public virtual void Add(T CustomerObject)
        {
            _innerArray.Add(CustomerObject);
        }
		
		public void Sort()
		{
			_innerArray.Sort();
		}

        // Remove first instance of a Customer object from the collection 
        public virtual bool Remove(T CustomerObject)
        {
            bool result = false;

            //loop through the inner array's indices
            for (int i = 0; i < _innerArray.Count; i++)
            {
                //store current index being checked
                T obj = (T)_innerArray[i];

                //compare the CustomerObjectBase UniqueId property
                if (obj.CustomerId == CustomerObject.CustomerId)
                {
                    //remove item from inner ArrayList at index i
                    _innerArray.RemoveAt(i);
                    result = true;
                    break;
                }
            }

            return result;
        }

        // Returns true/false based on whether or not it finds
        // the requested object in the collection.
        public bool Contains(T CustomerObject)
        {
            //loop through the inner ArrayList
            foreach (T obj in _innerArray)
            {
                //compare the CustomerObjectBase UniqueId property
                if (obj.CustomerId == CustomerObject.CustomerId)
                {
                    //if it matches return true
                    return true;
                }
            }
            //no match
            return false;
        }

        // Copy objects from this collection into another array
        public virtual void CopyTo(T[] CustomerObjectArray, int index)
        {
            throw new Exception(
              "This Method is not valid for this implementation.");
        }

        // Clear the collection of all it's elements
        public virtual void Clear()
        {
            _innerArray.Clear();
        }

        // Returns custom generic enumerator for this CustomerObjectCollection
        public virtual IEnumerator<T> GetEnumerator()
        {
            //return a custom enumerator object instantiated
            //to use this CustomerObjectCollection 
            return new CustomerObjectEnumerator<T>(this);
        }

        // Explicit non-generic interface implementation for IEnumerable
        // extended and required by ICollection (implemented by ICollection<T>)
        IEnumerator IEnumerable.GetEnumerator()
        {
            return new CustomerObjectEnumerator<T>(this);
        }
    }

    public class CustomerObjectEnumerator<T> : IEnumerator<T> where T : Customer
    {
        protected CustomerObjectCollection<T> _collection; //enumerated collection
        protected int index; //current index
        protected T _current; //current enumerated object in the collection

        // Default constructor
        public CustomerObjectEnumerator()
        {
            //nothing
        }

        // Paramaterized constructor which takes
        // the collection which this enumerator will enumerate
        public CustomerObjectEnumerator(CustomerObjectCollection<T> collection)
        {
            _collection = collection;
            index = -1;
            _current = default(T);
        }

        // Current Enumerated object in the inner collection
        public virtual T Current
        {
            get
            {
                return _current;
            }
        }

        // Explicit non-generic interface implementation for IEnumerator
        // (extended and required by IEnumerator<T>)
        object IEnumerator.Current
        {
            get
            {
                return _current;
            }
        }

        // Dispose method
        public virtual void Dispose()
        {
            _collection = null;
            _current = default(T);
            index = -1;
        }
		
        // Move to next element in the inner collection
        public virtual bool MoveNext()
        {
            //make sure we are within the bounds of the collection
            if (++index >= _collection.Count)
            {
                //if not return false
                return false;
            }
            else
            {
                //if we are, then set the current element
                //to the next object in the collection
                _current = _collection[index];
            }
            //return true
            return true;
        }

        // Reset the enumerator
        public virtual void Reset()
        {
            _current = default(T); //reset current object
            index = -1;
        }
}
namespace PublisherClass
{
	public delegate void EventGeneration(int count);

	public class Publisher
	{
		public static int count = 0;
		public event EventGeneration addEvent;
	
		public void showEvent()
		{
			if(addEvent!=null)
			{
				count++;
				addEvent(count);
			}
		}
	
		public void removeEvent()
		{
			if(addEvent!=null)
			{
				if(count>0)
				{
					count--;
					addEvent(count);
				}
			}
		}
	}
}